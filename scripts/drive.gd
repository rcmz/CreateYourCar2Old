extends Node2D

onready var car = $Car

func _ready():
	car.restore_from_file()
	car.build()
	Physics2DServer.set_active(true)
	

func on_build():
	get_tree().change_scene('res://scenes/build.tscn')
