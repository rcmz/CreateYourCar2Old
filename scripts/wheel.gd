extends RigidBody2D
class_name Wheel

onready var collision_shape = $CollisionShape2D
onready var sprite = $Sprite
onready var pin = $PinJoint2D
var descriptor:Dictionary

func descriptor_update():
	var cell_size = Grid.get_cell_size()
	position = descriptor.center * cell_size
	descriptor.radius = clamp(descriptor.radius, 1, 5)
	collision_shape.shape.radius = descriptor.radius * cell_size
	sprite.scale = Vector2.ONE * 2 * descriptor.radius * cell_size

func build():
	mass = PI * descriptor.radius*descriptor.radius * 0.1
	for c in get_parent().get_children():
		if c is Frame and c.has_point(global_position):
			pin.node_b = pin.get_path_to(c)
			break

func has_point(point:Vector2) -> bool:
	return point.distance_to(global_position) < collision_shape.shape.radius

func on_editor_touch(cell:Vector2):
	descriptor = {
		'type': 'wheel',
		'center': cell,
		'radius': 0,
	}
	descriptor_update()

func on_editor_drag(cell:Vector2):
	var delta = (cell - descriptor.center).abs()
	descriptor.radius = max(delta.x, delta.y)
	descriptor_update()

func on_editor_release(cell:Vector2):
	pass

func _integrate_forces(state):
	pass
