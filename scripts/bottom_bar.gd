extends HBoxContainer
tool

onready var category_bar = $CategoriesBar
onready var parts_bar = $PartsBar
var part:String
signal part_changed

func _ready():
	for c in category_bar.get_children():
		c.connect('pressed', self, 'populate_parts_bar', [c])
		if c.pressed:
			c.emit_signal('pressed')

func populate_parts_bar(category_button):
	for c in parts_bar.get_children():
		c.free()
	for p in category_button.parts:
		var c = preload('res://scenes/part_button.tscn').instance()
		parts_bar.add_child(c)
		c.text = p.to_upper().replace('_', ' ')
		c.connect('pressed', self, 'set', ['part', p])
		c.connect('pressed', self, 'emit_signal', ['part_changed'])
	if parts_bar.get_child_count() > 0:
		var c = parts_bar.get_child(0)
		c.pressed = true
		c.emit_signal('pressed')
